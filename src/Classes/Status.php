<?php
/**
 * Created by PhpStorm.
 * User: jeffe
 * Date: 14/07/2018
 * Time: 02:03
 */
namespace Jefferson\Classes;
/**Retorna a mensagem junto com o error code */
class Status
{
   private $status = [
        0 => 'success.',
        1 => 'undefined error code.',
        2 => 'this algarism doesn`t exists.',
        3 => 'eu nao tenho ideia do que voce esta falando.',
        4 => 'insufficient number of terms.',
        5 => 'this algarism already exists.',
        6 => 'invalid algarism.',
        7 => 'i can not read your affirmation, try again in the example format.',
        8 => 'i can not identify this metals [iron, silver, gold.',
        9 => 'to calculate the value of the metal is necessary in the maximum two coins in the sentence.',
        10 => 'invalid question try to use the example.',
        11 => 'invalid galaxy coin.',
    ];

    /**
     * @return array
     */
    public function getStatus($code)
    {
        if(array_key_exists($code, $this->status)){
            $response = array(
                'code' => $code,
                'message' => $this->status[$code]
            );
        } else {
            $response = array(
                'code' => 1,
                'message' => $this->status[1]
            );
        }
        return $response;
    }


}