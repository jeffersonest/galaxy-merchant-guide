<?php
/**
 * Created by PhpStorm.
 * User: Jefferson Estevam
 * Date: 13/07/2018
 * Time: 18:10
 */

namespace Jefferson\Classes;

use Jefferson\Classes\Status;

class Transitions
{
    /**
     * @var array containing roman values
     */
    public $roman_values = [
        //U
        'I' => 1,
        'II' => 2,
        'III' => 3,
        'IV' => 4,
        'V' => 5,
        'VI' => 6,
        'VII' => 7,
        'VIII' => 8,
        'IX' => 9,
        //D
        'X' => 10,
        'XI' => 11,
        'XII' => 12,
        'XIII' => 13,
        'XX' => 20,
        'XXX' => 30,
        'XL' => 40,
        'L' => 50,
        'LX' => 60,
        'LXX' => 70,
        'LXXX' => 80,
        'XC' => 90,
        //C
        'C' => 100,
        'CC' => 200,
        'CCC' => 300,
        'CD' => 400,
        'D' => 500,
        'DC' => 600,
        'DCC' => 700,
        'DCCC' => 800,
        'CM' => 900,
        //M
        'M' => 1000,
        'MM' => 2000,
        'MMM' => 3000,
    ];

    /**
     * @var array containing galaxy values
     */
    public $galaxy_coin = [
//        'glob' => "I",
//        'prok' => "V",
//        'pish' => "X",
//        'tegj' => "L",
          'glob' => null,
          'prok' => null,
          'pish' => null,
          'tegj' => null,
    ];

    /**
     * @var array containing metal values
     */
    protected $metal_values = [
//        'iron' => 195.5,
//        'silver' => 17,
//        'gold' => 14450
          'iron'   => null,
          'silver' => null,
          'gold'   => null
    ];


    protected $status;
    protected $question_repsonse;

    /**
     * Transitions constructor.
     */
    public function __construct()
    {
        $this->status = new Status();
    }

    /**
     * @return array
     */
    public function getGalaxyCoin(): array
    {
        return $this->galaxy_coin;
    }

    /**
     * @return mixed
     */
    public function getQuestionRepsonse()
    {
        return $this->question_repsonse;
    }

    /**
     * @param mixed $question_repsonse
     */
    public function setQuestionRepsonse($question_repsonse): void
    {
        $this->question_repsonse = $question_repsonse;
    }



    public function setGalaxyCoin($galaxy_algarism, $roman_algarism)
    {
        $result = false;

        if (array_key_exists(strtoupper($roman_algarism), $this->roman_values)       //verifica se existe esse algarismo romano
            && (array_key_exists(strtolower($galaxy_algarism), $this->galaxy_coin))) { //verifica se a moeda da galaxia existe

            if (!in_array(strtoupper($roman_algarism), $this->galaxy_coin)) { //verifica se o algarismom ja foi atribuido a alguma moeda da galaxia
                $this->galaxy_coin[$galaxy_algarism] = strtoupper($roman_algarism);
                $result = $this->status->getStatus(0);
            } else {
                $result = $this->status->getStatus(5);
            }

        } else {
            $result = $this->status->getStatus(6);
        }

        return $result;
    }


    public function validateRomanText($roman_string)
    {
        //https://www.regexplanet.com/cookbook/roman-numeral/index.html
        //Regex para checar a validade dos algarismos romanos
        $regex = '/^(M{0,3})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/';
        preg_match($regex, strtoupper($roman_string), $matches);

        if (!empty($matches)) { //verifica o retorno do regex
            $result = array_values(array_filter($matches)); //Limpa lixo gerado pelo regex e reordena o array
        } else {
            $result = false;
        }

        $result = $this->getRomanValues($result);

        return $result;
    }


    /**
     * @return array
     */
    public function getRomanValues($roman_array)
    {
        $roman_elements_sum = 0;

        if (!empty($roman_array) && !false) {

            foreach ($roman_array as $key => $letter) {

                $letter = strtoupper($letter);

                if (array_key_exists($letter, $this->roman_values) && ($key !== 0)) { //Pesquisa se existe o valor romano e atribui um valor
                    $result[$key]['algarism'] = $letter;
                    $result[$key]['value'] = $this->roman_values[$letter];
                } else { //se nao adiciona um valor invalido
                    $result[$key]['algarism'] = $letter;
                    $result[$key]['value'] = 0;
                }

                $roman_elements_sum = $roman_elements_sum + $result[$key]['value'];

            }

            $result[0]['value'] = $roman_elements_sum;
        } else {
            $result = false;
        }

        return $result;
    }

    public function calculateSentenceValue($sentence_value)
    {
        $result = null;
        $galaxy_coins = [];

        $affirmation_values = explode(" ", strtolower($sentence_value)); //quebra a sentenca em elementos

        if (count($affirmation_values) >= 4) { //verifica se a sentenca possui o minimo possivel, que seria somente o valor do metal

            $is_position = array_search('is', $affirmation_values, false); //captura a posicao do separador is
            $result = $this->status->getStatus(0);
            $credits = $affirmation_values[$is_position + 1];
            $credits_position = is_numeric($credits);//verifica se realmente a sentenca informa os creditos

            for ($i = 0; $i <= $is_position - 1; $i++) {
                if (array_key_exists(strtolower($affirmation_values[$i]), $this->galaxy_coin)) { //verifica se a moeda da galaxia existe
                    array_push($galaxy_coins, $affirmation_values[$i]);  //isola valores que podem ser aproveitados
                    if (count($galaxy_coins) > 2) { //forca o erro por excesso de moedas da galaxia para o calculo do valor do metal
                        $result = $this->status->getStatus(9);
                        goto ends;
                    }
                }
            }

            if (empty($galaxy_coins) || !$is_position || !$credits_position) { //verifica se foi possivel resgatar algum valor na sentenca
                $result = $this->status->getStatus(7);
            } else { //tenta capturar o nome do metal
                if (array_key_exists($affirmation_values[$is_position - 1], $this->metal_values)) {//verifica a existencia do metal para realizar a identificacao do seu valor

                    foreach ($galaxy_coins as $key => $coin) {
                        $galaxy_coins[$key] = $this->galaxy_coin[$coin]; //atribui o algarismo romano de acordo com a moeda da galaxia previamente atribuido
                    }

                    $roman_algarisms = implode($galaxy_coins);//transforma o array em uma expressao de algarismos romanos

                    $algarisms = $this->validateRomanText($roman_algarisms);//valida o algarismo romano gerado

                    if (!$algarisms) {
                        $result = $this->status->getStatus(6);
                    } else {
                        $roman_to_int = $algarisms[0]['value']; //sabe-se que na posicao zero do array retornado pelo convesor de algarismos romanos "getRomanValues" armazena o valor do algarismo.
                        $metal_value = ($credits / $roman_to_int);
                        $this->metal_values[$affirmation_values[$is_position - 1]] = $metal_value; //com o metal identificado, o indice eh reservado para receber o valor do metal
                        print_r($this->metal_values);
                    }

                } else {
                    $result = $this->status->getStatus(8);
                }
            }

        } else {
            $result = $this->status->getStatus(4);
        }
        ends:
        return $result;
    }

    public function calculateQuestionValue($sentence)
    {
        $result = null;
        $galaxy_coins = [];
        $galaxy_to_roman = [];

        if (!empty($sentence)) {
            $question_values = explode(" is ", strtolower($sentence)); //particiona a pergunta para veriricar se é válida a pergunta

            if (count($question_values) != 0) {
                $is_position = array_search('is', $question_values, false); //captura a posicao do separador is

                if (array_search('how much', $question_values, false) === 0) {
                    $galaxy_coins = explode(" ",$question_values[1]);

                    $question = end($galaxy_coins) == "?" ? true : false; //verifica se a posicao do ? esta correta

                    if($question){
                        array_pop($galaxy_coins);

                        foreach ($galaxy_coins as $key =>  $galaxy_coin){

                            if((array_key_exists(strtolower($galaxy_coin), $this->galaxy_coin))){ //verifica se as moedas da galaxya sao validas
                                array_push($galaxy_to_roman, $galaxy_coin); //adiciona ao array somente moedas
                            } else {
                                $result = $this->status->getStatus(11);
                                goto ends;
                            }

                        }

                        foreach ($galaxy_coins as $key => $coin) {
                            $galaxy_to_roman[$key] = $this->galaxy_coin[$coin]; //atribui o algarismo romano de acordo com a moeda da galaxia previamente atribuido
                        }

                        $roman_algarisms = implode($galaxy_to_roman);//transforma o array em uma expressao de algarismos romanos

                        $algarisms = $this->validateRomanText($roman_algarisms);//valida o algarismo romano gerado

                        if (!$algarisms) {
                            $result = $this->status->getStatus(6);
                        } else {
                            $roman_to_int = $algarisms[0]['value']; //sabe-se que na posicao zero do array retornado pelo convesor de algarismos romanos "getRomanValues" armazena o valor do algarismo.
                            $this->setQuestionRepsonse($roman_to_int);
                            $result = $this->status->getStatus(0);
                        }

                    } else {
                        $result = $this->status->getStatus(10);
                        goto ends;
                    }

                } elseif (array_search('how many credits', $question_values, false) === 0) {
                    $galaxy_coins = explode(" ",$question_values[1]);
                    $question = end($galaxy_coins) == "?" ? true : false; //verifica se a posicao do ? esta correta

                    if($question){
                        array_pop($galaxy_coins);

                        foreach ($galaxy_coins as $key =>  $galaxy_coin){

                            if((array_key_exists(strtolower($galaxy_coin), $this->galaxy_coin))){ //verifica se as moedas da galaxya sao validas
                                array_push($galaxy_to_roman, $galaxy_coin); //adiciona ao array somente moedas
                            } else if((array_key_exists(strtolower($galaxy_coin), $this->metal_values))) { //verifica se o metal é valido e captura o seu nome
                                $metal_name = $galaxy_coins[$key];
                                unset($galaxy_coins[$key]); //remove metal do array de moedas da galaxia
                            } else {
                                $result = $this->status->getStatus(11);
                                goto ends;
                            }

                        }

                        foreach ($galaxy_coins as $key => $coin) {
                            $galaxy_to_roman[$key] = $this->galaxy_coin[$coin]; //atribui o algarismo romano de acordo com a moeda da galaxia previamente atribuido
                        }

                        $roman_algarisms = implode($galaxy_to_roman);//transforma o array em uma expressao de algarismos romanos

                        $algarisms = $this->validateRomanText($roman_algarisms);//valida o algarismo romano gerado

                        if (!$algarisms) {
                            $result = $this->status->getStatus(6);
                        } else {
                            $roman_to_int = $algarisms[0]['value']; //sabe-se que na posicao zero do array retornado pelo convesor de algarismos romanos "getRomanValues" armazena o valor do algarismo.
                            $this->setQuestionRepsonse($roman_to_int * $this->metal_values[$metal_name]); //adiciona o valor do metal a resposta
                            $result = $this->status->getStatus(0);
                        }

                    } else {
                        $result = $this->status->getStatus(10);
                        goto ends;
                    }


                } else {
                    $result = $this->status->getStatus(10);
                }
            } else {
                $result = $this->status->getStatus(10);
            }
        } else {
            $result = $this->status->getStatus(10);
        }
        ends:
        return $result;
    }

}