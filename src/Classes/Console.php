<?php
/**
 * Created by PhpStorm.
 * User: jeffe
 * Date: 15/07/2018
 * Time: 03:36
 */

namespace Jefferson\Classes;

class Console
{

    public function readline()
    {
        return rtrim(fgets(STDIN));
    }
}