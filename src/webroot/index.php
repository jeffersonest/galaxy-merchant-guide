<?php
require_once "../../vendor/autoload.php";

use Jefferson\Classes\Transitions;

$transitions = new Transitions();
$results = $transitions->validateRomanText('DCCLXXVII');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galaxy Guide</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <style>
        body {
            padding: 0;
            margin: 0;
        }

        .white {
            color: white !important;
        }

        #galaxy {
            z-index: 0;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #000000;
            -webkit-transition: background 0.8s ease-out;
            -moz-transition: background 0.8s ease-out;
            -o-transition: background 0.8s ease-out;
            -ms-transition: background 0.8s ease-out;
            transition: background 0.8s ease-out;
        }

        #galaxy,
        #galaxy .bg {
            width: 100%;
            height: 100%;
        }

        #galaxy * {
            position: absolute;
        }

        #galaxy .bg {
            /* background: url('https://raw.githubusercontent.com/gurde/css3-animated-galaxy/master/img/bg.jpg') no-repeat 50% 50%; */
        }

        #galaxy .bg.center {
            top: 50%;
            left: 50%;
            width: 400px;
            height: 200px;
            margin: -100px 0 0 -200px;
            opacity: 0.8;
            -webkit-border-radius: 500px;
            -moz-border-radius: 500px;
            border-radius: 500px;
        }

        #galaxy [class^="stars"] {
            top: -50%;
            left: -50%;
            width: 200%;
            height: 200%;
        }

        #galaxy .stars-back {
            background: url("https://raw.githubusercontent.com/gurde/css3-animated-galaxy/master/img/back.png");
            -webkit-animation: orbit-clock-wise 440s infinite linear;
            -moz-animation: orbit-clock-wise 440s infinite linear;
            -o-animation: orbit-clock-wise 440s infinite linear;
            -ms-animation: orbit-clock-wise 440s infinite linear;
            animation: orbit-clock-wise 440s infinite linear;
        }

        #galaxy .stars-middle {
            background: url("https://raw.githubusercontent.com/gurde/css3-animated-galaxy/master/img/middle.png");
            -webkit-animation: orbit-counter-clock-wise 360s infinite linear;
            -moz-animation: orbit-counter-clock-wise 360s infinite linear;
            -o-animation: orbit-counter-clock-wise 360s infinite linear;
            -ms-animation: orbit-counter-clock-wise 360s infinite linear;
            animation: orbit-counter-clock-wise 360s infinite linear;
        }

        #galaxy .stars-front {
            background: url("https://raw.githubusercontent.com/gurde/css3-animated-galaxy/master/img/front.png");
            -webkit-animation: orbit-clock-wise 160s infinite linear;
            -moz-animation: orbit-clock-wise 160s infinite linear;
            -o-animation: orbit-clock-wise 160s infinite linear;
            -ms-animation: orbit-clock-wise 160s infinite linear;
            animation: orbit-clock-wise 160s infinite linear;
        }

        @-webkit-keyframes orbit-clock-wise {
            0% {
                opacity: 0.4;
                -webkit-transform: rotate(0deg);
            }
            2% {
                opacity: 0.8;
            }
            4% {
                opacity: 0.2;
            }
            5% {
                opacity: 0.8;
            }
            100% {
                opacity: 0.4;
                -webkit-transform: rotate(360deg);
            }
        }
        @-webkit-keyframes orbit-counter-clock-wise {
            from {
                -webkit-transform: rotate(360deg);
            }
            to {
                -webkit-transform: rotate(0deg);
            }
        }
        @-moz-keyframes orbit-clock-wise {
            0% {
                opacity: 0.4;
                -moz-transform: rotate(0deg);
            }
            2% {
                opacity: 0.8;
            }
            4% {
                opacity: 0.2;
            }
            5% {
                opacity: 0.8;
            }
            100% {
                opacity: 0.4;
                -moz-transform: rotate(360deg);
            }
        }
        @-moz-keyframes orbit-counter-clock-wise {
            from {
                -moz-transform: rotate(360deg);
            }
            to {
                -moz-transform: rotate(0deg);
            }
        }
        @-o-keyframes orbit-clock-wise {
            0% {
                opacity: 0.4;
                -o-transform: rotate(0deg);
            }
            2% {
                opacity: 0.8;
            }
            4% {
                opacity: 0.2;
            }
            5% {
                opacity: 0.8;
            }
            100% {
                opacity: 0.4;
                -o-transform: rotate(360deg);
            }
        }
        @-o-keyframes orbit-counter-clock-wise {
            from {
                -o-transform: rotate(360deg);
            }
            to {
                -o-transform: rotate(0deg);
            }
        }
        @-ms-keyframes orbit-clock-wise {
            0% {
                opacity: 0.4;
                -ms-transform: rotate(0deg);
            }
            2% {
                opacity: 0.8;
            }
            4% {
                opacity: 0.2;
            }
            5% {
                opacity: 0.8;
            }
            100% {
                opacity: 0.4;
                -ms-transform: rotate(360deg);
            }
        }
        @-ms-keyframes orbit-counter-clock-wise {
            from {
                -ms-transform: rotate(360deg);
            }
            to {
                -ms-transform: rotate(0deg);
            }
        }
        @keyframes orbit-clock-wise {
            0% {
                opacity: 0.4;
                transform: rotate(0deg);
            }
            2% {
                opacity: 0.8;
            }
            4% {
                opacity: 0.2;
            }
            5% {
                opacity: 0.8;
            }
            100% {
                opacity: 0.4;
                transform: rotate(360deg);
            }
        }
        @keyframes orbit-counter-clock-wise {
            from {
                transform: rotate(360deg);
            }
            to {
                transform: rotate(0deg);
            }
        }

    </style>
</head>
<body>
    <div id="galaxy">
        <div class="bg"></div>
        <div class="stars-back"></div>
        <div class="stars-middle"></div>
        <div class="stars-front"></div>
        <div class="bg center"></div>
    </div>
    <section class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-center">
               <h1 class="white">Merchant's Guide to the Galaxy</h1>

                    <?php

                        if(!empty($results)){
                            foreach ($results as $result){
                                echo "<p class='white'>" . $result['algarism'] . " = " . $result['value'] . "</p>";
                            }
                        } else {
                            echo "<p class='white'>Invalid value</p>";
                        }

                    ?>

            </div>
            <div class="col-md-2"></div>
        </div>
    </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>