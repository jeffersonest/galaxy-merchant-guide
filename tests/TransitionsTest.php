<?php
/**
 * Created by PhpStorm.
 * User: jeffe
 * Date: 16/07/2018
 * Time: 18:44
 */

use Jefferson\Classes\Transitions;
use PHPUnit\Framework\TestCase;

class TransitionsTest extends TestCase
{

    public function testGetRomanValues()
    {

        $transitions = new Transitions();

        $roman_values = [
            0 => 'DCCLXXVII', //first position is the complete algarism
            1 => 'DCC',
            2 => 'LXX',
            3 => 'VII'
        ];

        $response_array = array
        (
            0 => array
            (
                "algarism" => "DCCLXXVII",
                "value" => 777,
            ),
            1 => array
            (
                "algarism" => "DCC",
                "value" => 700,
            ),
            2 => array
            (
                "algarism" => "LXX",
                "value" => 70,
            ),
            3 => array
            (
                "algarism" => "VII",
                "value" => 7,
            )
        );

        $algarism_value = $transitions->getRomanValues($roman_values);

        $this->assertEquals($response_array, $algarism_value);

    }

    public function testCalculateQuestionValue()
    {

    }

    public function testGetGalaxyCoin()
    {

    }

    public function testCalculateSentenceValue()
    {

    }

    public function testSetQuestionRepsonse()
    {

    }

    public function testSetGalaxyCoin()
    {

    }

    public function testGetQuestionRepsonse()
    {

    }

    public function testValidateRomanText()
    {

    }
}
