<?php
require_once "./vendor/autoload.php";

use Jefferson\Classes\Console;
use Jefferson\Classes\Transitions;

$transitions = new Transitions();

$line = new Console();

echo "Welcome to Merchant's Galaxy Guide!\n";
echo "Enter the Sentenses: \n";
glob:
    echo "1- glob is: ";
    $roman_glob_value = $line->readline();
    $result = $transitions->setGalaxyCoin('glob', $roman_glob_value);
    if($result['code'] != 0){
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto glob;
    }
prok:
    echo "2- prok is: ";
    $roman_prok_value = $line->readline();
    $result = $transitions->setGalaxyCoin('prok', $roman_prok_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto prok;
    }
pish:
    echo "3- pish is: ";
    $roman_pish_value = $line->readline();
    $result = $transitions->setGalaxyCoin('pish', $roman_pish_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto pish;
    }
tegj:
    echo "4- tegj is: ";
    $roman_tegj_value = $line->readline();
    $result = $transitions->setGalaxyCoin('tegj', $roman_tegj_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto tegj;
    }

echo "\nI need some information, so...";
echo "\nHelp me to discover the metal values, add the credit values to the sentences:\n";

silver_value:
    echo "Write sentence to discover the silver value,\n";
    echo "Max, 2 coins, ex: glob glob.\n";
    echo "Ex: glob glob silver is 34 credits\n";
    echo "Sentence: ";
    $silver_value = $line->readline();
    $result = $transitions->calculateSentenceValue($silver_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto silver_value;
    }

gold_value:
    echo "Write sentence to discover the gold value,\n";
    echo "Max, 2 coins, ex: glob prok.\n";
    echo "Ex: glob prok gold is 57800 credits\n";
    echo "Sentence: ";
    $gold_value = $line->readline();
    $result = $transitions->calculateSentenceValue($gold_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto gold_value;
    }

iron_value:
    echo "Write sentence to discover the gold value,\n";
    echo "Max, 2 coins, ex: pish pish.\n";
    echo "Ex: pish pish iron is 3910 credits\n";
    echo "Sentence: ";
    $iron_value = $line->readline();
    $result = $transitions->calculateSentenceValue($iron_value);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto iron_value;
    }

echo "Perfect! now I know the values ​​of metals [gold, silver, iron]\n";
echo "Now let's calculate.\n";

echo "Perfect! now I know the values ​​of metals [gold, silver, iron]\n";
echo "Now let's calculate or press ctrl+c to exit.\n";

question_types:
    echo "Write your question like the examples:\n";
    echo "  how many Credits is glob prok Silver ? \n";
    echo "  how much is pish tegj glob glob ? \n";
    echo "  how many Credits is glob prok Gold ? \n";
    echo "  how many Credits is glob prok Iron ? \n\n";
    $sentence = $line->readline();
    $result = $transitions->calculateQuestionValue($sentence);
    if ($result['code'] != 0) {
        echo "error: ".$result['code'].", message: ".$result['message']."\n";
        goto question_types;
    } else {
        echo "Your response is: " . $transitions->getQuestionRepsonse() . "\n";
        goto question_types;
    }

